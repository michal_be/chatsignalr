﻿using ChatSignalR.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ChatSignalR.Entities;

namespace ChatSignalR.Repositories
{
    public class ChatRepository : IChatRepository
    {
        private readonly ApplicationDbContext _context;

        public ChatRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public bool CheckIfConnectionExists(string currentUserId, string userToChatId)
        {
            var connectionExists = _context.ChatConnections.
                Any(x => (x.FirstUserId == currentUserId && x.SecondUserId == userToChatId)
                         || (x.FirstUserId == userToChatId && x.SecondUserId == currentUserId));

            return connectionExists;
        }

        public long GetConnectionId(string currentUserId, string userToChatId)
        {
            var id = _context.ChatConnections.
                Where(x => (x.FirstUserId == currentUserId && x.SecondUserId == userToChatId)
                           || (x.FirstUserId == userToChatId && x.SecondUserId == currentUserId)).
                Select(x => x.ConnectionId).
                FirstOrDefault();

            return id;
        }

        public long SetConnectionAndReturnId(string currentUserId, string userToChatId)
        {
            var connection = new ChatConnection()
            {
                FirstUserId = currentUserId,
                SecondUserId = userToChatId
            };

            _context.ChatConnections.Add(connection);
            _context.SaveChanges();

            return connection.ConnectionId;
        }

        public void SaveMessageToDb(ChatMessage chatMessage)
        {
            _context.ChatMessages.Add(chatMessage);
            _context.SaveChanges();
        }

        public List<ChatMessage> GetMessagesFromDb(long connectionId)
        {
            var messages = _context.ChatMessages.Where(x => x.ConnectionId == connectionId).OrderBy(x => x.Date).ToList();
            return messages;
        } 
    }
}