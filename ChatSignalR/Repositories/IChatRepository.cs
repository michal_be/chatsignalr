﻿using System.Collections.Generic;
using ChatSignalR.Entities;

namespace ChatSignalR.Repositories
{
    public interface IChatRepository
    {
        bool CheckIfConnectionExists(string currentUserId, string userToChatId);
        long GetConnectionId(string currentUserId, string userToChatId);
        long SetConnectionAndReturnId(string currentUserId, string userToChatId);
        void SaveMessageToDb(ChatMessage chatMessage);
        List<ChatMessage> GetMessagesFromDb(long connectionId);
    }
}