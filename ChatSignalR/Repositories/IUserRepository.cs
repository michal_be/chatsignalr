﻿using System.Collections.Generic;
using ChatSignalR.Entities;
using ChatSignalR.Models;


namespace ChatSignalR.Repositories
{
    public interface IUserRepository
    {
        List<ApplicationUser> GetUsers();
        string GetUserFullNameById(string userId);
        ApplicationUser GetUserById(string userId);
    }
}