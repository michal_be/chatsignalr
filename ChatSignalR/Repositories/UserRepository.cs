﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ChatSignalR.Models;
using ChatSignalR.Entities;

namespace ChatSignalR.Repositories
{
    public class UserRepository : IUserRepository
    {
        private ApplicationDbContext _context;

        public UserRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public List<ApplicationUser> GetUsers()
        {
            var users = _context.Users.ToList();
            return users;
        }

        public string GetUserFullNameById(string userId)
        {
            var fullName = _context.Users.Where(u => u.Id == userId).Select(u => u.FullName).FirstOrDefault();
            return fullName;
        }
    
        public ApplicationUser GetUserById(string userId)
        {
            var user = _context.Users.FirstOrDefault(u => u.Id == userId);
            return user;
        }
    }
}