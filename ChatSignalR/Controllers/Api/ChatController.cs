﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ChatSignalR.Services;

namespace ChatSignalR.Controllers.Api
{
    [Authorize]
    public class ChatController : ApiController
    {
        private readonly IChatService _chatService;

        public ChatController(IChatService chatService)
        {
            _chatService = chatService;
        }

        [Route("api/chat/getMessagesFromDb/{connectionId}")]
        [HttpGet]
        public IHttpActionResult GetMessagesFromDb(long connectionId)
        {
            var messages = _chatService.GetMessagesFromDb(connectionId);
            return Ok(messages);
        }
    }
}