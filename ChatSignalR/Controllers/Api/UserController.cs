﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ChatSignalR.Services;
using System.Web;
using Microsoft.AspNet.Identity;
using System.Security.Claims;
using System.Web;
using System.Web.Http.Results;

namespace ChatSignalR.Controllers.Api
{
    [Authorize]
    public class UserController : ApiController
    {
        private readonly IUserService _userService;
 
        public UserController(IUserService userService)
        {
            _userService = userService;        
        }

        [Route("api/user/getFullName")]
        [HttpGet]
        public JsonResult<string> GetUserFullName()
        {
            string fullName = _userService.GetCurrentUserFullName();

            return Json(fullName);
        }

        [Route("api/user/trial")]
        [HttpGet]
        public IHttpActionResult GetData()
        {
            string data = "feeefe";
            return Ok(data);
        }


    }
}