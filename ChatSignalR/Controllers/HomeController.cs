﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ChatSignalR.Entities;
using ChatSignalR.Models;
using ChatSignalR.Services;
using Microsoft.AspNet.Identity;

namespace ChatSignalR.Controllers
{
    public class HomeController : Controller
    {
        private readonly UserService _userService;
        private readonly ChatService _chatService;
        public HomeController(UserService userService, ChatService chatService)
        {
            _userService = userService;
            _chatService = chatService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [Authorize]
        [HttpGet]
        public ActionResult Chat()
        {
            var usersList = _userService.GetUsers();            
            return View(usersList);
        }

        [Authorize]
        [HttpGet]
        public ActionResult ChatRoom(string userId)
        {
            var currentUserId = HttpContext.User.Identity.GetUserId();
            var userToChat = _userService.GetUserById(userId);
            var connectionId = _chatService.SetConnection(currentUserId, userToChat.Id);
            var viewModel = new ChatViewModel() {ConnectionId = connectionId, UserToChat = userToChat};

            return View(viewModel);
        }

    }
}