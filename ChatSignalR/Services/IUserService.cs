﻿using System.Collections.Generic;
using ChatSignalR.Entities;
using ChatSignalR.Models;

namespace ChatSignalR.Services
{
    public interface IUserService
    {
        List<ApplicationUser> GetUsers();
        string GetCurrentUserFullName();
    }
}