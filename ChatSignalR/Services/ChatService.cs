﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ChatSignalR.Repositories;
using ChatSignalR.Entities;

namespace ChatSignalR.Services
{
    public class ChatService : IChatService
    {
        private readonly IChatRepository _chatRepository;
        public ChatService(IChatRepository chatRepository)
        {
            _chatRepository = chatRepository;
        }

        public long SetConnection(string currentUserId, string userToChatId)
        {
            var connectionExists = _chatRepository.CheckIfConnectionExists(currentUserId, userToChatId);

            if (connectionExists)
            {
              var connectionId = _chatRepository.GetConnectionId(currentUserId, userToChatId);
              return connectionId;
            }

            var newConnectionId = _chatRepository.SetConnectionAndReturnId(currentUserId, userToChatId);
            return newConnectionId;
        }

        public void SaveMessageToDb(long connectionId, string currentUserId, string message)
        {
            var chatMessage = new ChatMessage()
            {
                ConnectionId = connectionId,
                AuthorUserId = currentUserId,
                MessageBody = message,
                Date = DateTime.Now
            };
            _chatRepository.SaveMessageToDb(chatMessage);
        }

        public List<ChatMessage> GetMessagesFromDb(long connectionId)
        {
            var message = _chatRepository.GetMessagesFromDb(connectionId);
            return message;
        }
    }
}