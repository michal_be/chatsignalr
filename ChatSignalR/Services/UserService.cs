﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ChatSignalR.Models;
using ChatSignalR.Repositories;
using ChatSignalR.Entities;
using Microsoft.AspNet.Identity;

namespace ChatSignalR.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public List<ApplicationUser> GetUsers()
        {
           return _userRepository.GetUsers();
        }

        public string GetCurrentUserFullName()
        {
            var userId = HttpContext.Current.GetOwinContext().Authentication.User.Identity.GetUserId();
            var fullName = _userRepository.GetUserFullNameById(userId);
            return fullName;
        }

        public ApplicationUser GetUserById(string userId)
        {
            var user = _userRepository.GetUserById(userId);
            return user;
        }

    }
}