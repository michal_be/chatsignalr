﻿using System.Collections.Generic;
using ChatSignalR.Entities;

namespace ChatSignalR.Services
{
    public interface IChatService
    {
        long SetConnection(string currentUserId, string userToChatId);
        void SaveMessageToDb(long connectionId, string currentUserId, string message);
        List<ChatMessage> GetMessagesFromDb(long connectionId);
    }
}