﻿
 var userToChatId, connectionId;

    $("#txtMsg").val('');

    var chatProxy = $.connection.chatHub;

    $.connection.hub.start().done(function() {

        getMessagesFromDb();

        $("#btnSend").click(function() {
            chatProxy.server.sendMessage(userToChatId , $("#txtMsg").val() , connectionId);
            var msg = $("#txtMsg").val();
            $('#txtMsg').val('').focus();
            showSentMessage(msg);
        });
    });

    chatProxy.client.receiveMessage = function(msg, cntId) {
        if (cntId === connectionId) {
            showReceivedMessage(msg);
    }
    };


    function getMessagesFromDb() {

        $.ajax({
            method: "GET",
            url: "/api/chat/getMessagesFromDb/" + connectionId,
            contentType: "application/json; charset=utf-8",
            success: function (result) {
                var data = result;
                viewMessages(data);
            }
        });
    };



    function viewMessages(data) {

        for (var i=0;i<data.length;i++) {
            if (data[i].AuthorUserId === userToChatId)
            {
                showReceivedMessage(data[i].MessageBody);
            }
            else {
                showSentMessage(data[i].MessageBody);
            }
        }
    };


    function showReceivedMessage(msg) {

        var linesCount = msg.split(/\r\n|\r|\n/).length;
        msg = msg.replace(/\r?\n/g, '<br />');
        var div = document.createElement('div');
        div.innerHTML = msg;
        div.setAttribute('class', 'message-received');

        $('#divChat').append(div);
        for (i = 0; i < linesCount; i++) {
            $('#divChat').append("<br />");
        }
    };


    function showSentMessage(msg) {

        var linesCount = msg.split(/\r\n|\r|\n/).length;
        msg = msg.replace(/\r?\n/g, '<br />');
        var div = document.createElement('div');
        div.innerHTML = msg;
        div.setAttribute('class', 'message-sent');

        $('#divChat').append(div);
        for (i = 0; i < linesCount; i++) {
        $('#divChat').append("<br />");
        }
    };


