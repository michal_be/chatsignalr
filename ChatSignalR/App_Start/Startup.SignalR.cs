﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ChatSignalR.Providers;
using Microsoft.AspNet.SignalR;
using Owin;

namespace ChatSignalR
{
    public partial class Startup
    {
        public void ConfigureSignalR(IAppBuilder app)
        {
            GlobalHost.DependencyResolver.Register(typeof(IUserIdProvider), () => new UserIdProvider());

            var configuration = new HubConfiguration();
            app.MapSignalR("/signalr", configuration);
        }
    }
}