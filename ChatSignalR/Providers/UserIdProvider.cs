﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.SignalR;

namespace ChatSignalR.Providers
{
    public class UserIdProvider : IUserIdProvider
    {
        public string GetUserId(IRequest request)
        {
            var userIdentity = request.User.Identity;
            var userId = userIdentity.GetUserId();
            return userId;
        }
    }
}