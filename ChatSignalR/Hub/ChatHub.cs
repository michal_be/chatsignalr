﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using ChatSignalR.Services;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace ChatSignalR.Hub
{
    [Authorize]
    [HubName("chatHub")]
    public class ChatHub : Microsoft.AspNet.SignalR.Hub
    {
        private readonly IChatService _chatService;

        public ChatHub(IChatService chatService)
        {
            _chatService = chatService;
        }

        public void SendMessage(string userDestId, string msg, long connectionId)
        {
            Clients.User(userDestId).receiveMessage(msg, connectionId);
            var currentUserId = HttpContext.Current.User.Identity.GetUserId();
            _chatService.SaveMessageToDb(connectionId, currentUserId, msg);
        }

    }
}