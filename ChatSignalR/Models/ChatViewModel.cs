﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ChatSignalR.Entities;

namespace ChatSignalR.Models
{
    public class ChatViewModel
    {
        public long ConnectionId { get; set; }
        public ApplicationUser UserToChat { get; set; }
  
    }
}