﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using ChatSignalR.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace ChatSignalR.Models
{
  
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<ChatConnection> ChatConnections { get; set; } 
        public DbSet<ChatMessage> ChatMessages { get; set; }

        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}