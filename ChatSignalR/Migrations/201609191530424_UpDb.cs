namespace ChatSignalR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpDb : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "FullName", c => c.String());
            AddColumn("dbo.AspNetUsers", "FacebookUserId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "FacebookUserId");
            DropColumn("dbo.AspNetUsers", "FullName");
        }
    }
}
