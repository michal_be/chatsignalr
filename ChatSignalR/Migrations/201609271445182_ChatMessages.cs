namespace ChatSignalR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChatMessages : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ChatMessages",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ConnectionId = c.Long(nullable: false),
                        AuthorUserId = c.String(),
                        MessageBody = c.String(),
                        Date = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ChatMessages");
        }
    }
}
