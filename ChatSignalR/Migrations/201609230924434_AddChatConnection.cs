namespace ChatSignalR.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddChatConnection : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ChatConnections",
                c => new
                    {
                        ConnectionId = c.Long(nullable: false, identity: true),
                        FirstUserId = c.String(),
                        SecondUserId = c.String(),
                    })
                .PrimaryKey(t => t.ConnectionId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ChatConnections");
        }
    }
}
