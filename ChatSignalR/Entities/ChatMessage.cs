﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ChatSignalR.Entities
{
    public class ChatMessage
    {
        [Key]
        public long Id { get; set; }
        public long ConnectionId { get; set; }
        public string AuthorUserId { get; set; }
        public string MessageBody { get; set; }
        public DateTime Date { get; set; }

    }
}