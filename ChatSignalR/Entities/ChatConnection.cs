﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ChatSignalR.Entities
{
    public class ChatConnection
    {
        [Key]
        public long ConnectionId { get; set; }
        public string FirstUserId { get; set; }
        public string SecondUserId { get; set; }
    }
}