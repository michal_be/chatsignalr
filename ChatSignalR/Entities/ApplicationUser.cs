﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ChatSignalR.Entities
{
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser()
        {
     
        }
        public string FullName { get; set; }
        public string FacebookUserId { get; set; }      
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            userIdentity.AddClaim(new Claim("FullName", FullName));
            userIdentity.AddClaim(new Claim("FirstName", FullName.Split(' ').First()));
            userIdentity.AddClaim(new Claim("FacebookUserId", FacebookUserId));
            userIdentity.AddClaim(new Claim("Email", Email));
            return userIdentity;
        }
    }
}